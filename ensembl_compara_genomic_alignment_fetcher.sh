#!/bin/bash
#
# bash ensembl_compara_genomic_alignment_fetcher.sh
# 
# Izaskun Mallona, GPL
# 5th Aug 2013

# The alignment types
:<<EOF 
  548 stands for 6 primates EPO
  619 for 13 eutherian mammals EPO
  630 for 20 ammniota vertebrates PECAN
  622 for 36 eutherian mammals LOW COVERAGE EPO
EOF

ALIGN="548 619 622"
ENSEMBL_BASE_URL="http://www.ensembl.org/Homo_sapiens/Export/Output/Location/"
# alujb, alusc8, cpgi, mini
TEST_LOCATIONS="9:6646386-6646676 9:6646758-6647031 9:6645081-6645975 9:6646869-6646951"
#TMP="$HOME"/tmp/gldc_tmp
JASPAR_MATRICES="$IHOME"/data/jaspar/motifVertebratesNonReduntant.meme
FIMO_PARSER="$IHOME"/src/compara_tfbs/fimo_output_parser.R
HG19_ALUS="$IHOME"/data/alus/hg_19_track_repeatMasker_table_rmsk_repFamily_matches_Alu.bed
HG19_FASTA=/biodata/indices/species/Hsapiens/ucsc.hg19.fa

ALUSC_FASTA="$IHOME"/map/gldc/data/alusc8_gldc_hg19.fa
ALUJB_FASTA="$IHOME"/map/gldc/data/alujb_gldc_hg19.fa

action=$1

function usage() {
    echo ""
    echo "Fetches the ENSEMBL compara data genomic assemblies of a given genomic hg19 coordinate set"
    echo ""
    echo "USAGE:"
    echo ""
    echo "  bash ensembl_compara_genomic_alignment_fetcher.sh <type> [<loc>] [<filetype>] <working_dir>"
    echo ""
    echo "    being <type>: 'query', 'test' or 'tfbs'"
    echo ""
    echo "Examples:"
    echo "    User-defined genomic locations and filetype"
    echo "       bash ensembl_compara_genomic_alignment_fetcher.sh query 9:6645081-6647031 fasta /tmp"
    echo "    Predefined test (forces fasta format)"
    echo "       bash ensembl_compara_genomic_alignment_fetcher.sh test /tmp"
    echo "    TFBS enrichment analysis of the test data (forces fasta format)"
    echo "       bash ensembl_compara_genomic_alignment_fetcher.sh tfbs /tmp"
    exit 1
}

function wget_genomic_alignment() {
    for align in ${ALIGN[@]};
    do
	curr="Alignment?align=${align}&db=core;output=alignment&r=${1};format=${2};_format=Text"
	curr_url="${ENSEMBL_BASE_URL}${curr}"
	wget "$curr_url"
	
	# summaryzing playground start    
	if [ "$2" == "fasta" ];
	then
	    #gawk 'match($0, /^>(\w+)\d?\//, elem) {print elem[1]}' "$curr" > "${1}"_"${align}"_species_matches.dat
	    gawk 'match($0, /^>([a-z_A-Z]+)\d?\//, elem) {print elem[1]}' "$curr" > "${1}"_"${align}"_species_matches.dat
	fi
	# summaryzing playground end
    done
    
    if [ "$3" == "fasta" ];
    then
	# senseless if the formart is not fasta
	cat *"${1}"* > "${1}"_all_merged.fasta
	
        # carriage returns
	dos2unix "${1}"_all_merged.fasta
	
        # blank lines
	awk 'NF>0' "${1}"_all_merged.fasta > "${1}"_all_merged.fasta2
	mv "${1}"_all_merged.fasta2 "${1}"_all_merged.fasta
	
        # removing duplicates (untested recipe found elsewhere)
	sed -e '/^>/s/$/@/' -e 's/^>/#/' "${1}"_all_merged.fasta \
	    | tr -d '\n' | tr "#" "\n" | tr "@" "\t" \
	    | sort -u -t $'\t' -f -k 2,2  | awk 'NF>0'  \
	    | sed -e 's/^/>/' -e 's/\t/\n/' > "${1}"_all_merged_unique.fasta
	
    fi
}

function wget_genomic_alignment_new() {
    for align in ${ALIGN[@]};
    do
	for loc in ${TEST_LOCATIONS[@]};
	do
	    curr="Alignment?align=${align}&db=core;output=alignment&r=${loc};format=fasta;_format=Text"
	    curr_url="${ENSEMBL_BASE_URL}${curr}"

	    pattern="([0-9]+):([0-9]+)-([0-9]+)_*"

	    [[ $loc =~ $pattern ]]
	    chr="${BASH_REMATCH[1]}"
            start="${BASH_REMATCH[2]}"
            end="${BASH_REMATCH[3]}"

	    wget "$curr_url" -O chr"$chr"_from_"$start"_to_"$end"_align_"$align"_gapped.fasta

	    degapseq \
		-sequence chr"$chr"_from_"$start"_to_"$end"_align_"$align"_gapped.fasta \
		-outseq chr"$chr"_from_"$start"_to_"$end"_retrieved_from_"$align".fasta

	    fimo --verbosity 1 \
		--oc "$OUT"/"$loc"_"$align" \
		"$JASPAR_MATRICES"  \
		chr"$chr"_from_"$start"_to_"$end"_retrieved_from_"$align".fasta
	    
	    Rscript $FIMO_PARSER \
		"$OUT"/"$loc"_"$align"/fimo.txt \
		"$OUT"/"$loc"_"$align"/"$loc"_"$align"_fimo_parsed.tsv
	done
    done
}

if [ $# = 0 ]
then
    usage
fi

case "$action" in
    query)
	OUT=$4
	wget_genomic_alignment $2 $3
	;;
    test)
	OUT=$2
	for loc in ${TEST_LOCATIONS[@]};
	do
	    wget_genomic_alignment "${loc}" "fasta"
	done
	
	for align in ${ALIGN[@]};
	do
	    echo "do some matrix generation of shared species here"
	done
	;;
    tfbs)
	OUT=$2
	mkdir -p $OUT
	cd "$OUT"
	wget_genomic_alignment_new
	
	# pastes all the summaries in a non-structured way
	rm "$OUT"/summary.tsv
	for file in `find "$OUT" -name '*parsed*' -type f`;
	do
	    echo $file >> "$OUT"/summary.tsv
	    cat $file >> "$OUT"/summary.tsv
	done
	;;
    
    # new background-family-based approach
    family)
	OUT=$2
	mkdir -p $OUT
	cd "$OUT"
	
	# picking a sample of 1000 Alus of each type
	grep AluSc "$HG19_ALUS" | sort --random-sort | head -n 30 > "$OUT"/sampled_hg19_AluSc.bed
	grep AluJb "$HG19_ALUS" | sort --random-sort | head -n 30 > "$OUT"/sampled_hg19_AluJb.bed	

	###
	
	fastaFromBed -s -fi "$HG19_FASTA" \
	    -bed "$OUT"/sampled_hg19_AluSc.bed \
	    -fo "$OUT"/sampled_hg19_AluSc.fasta	

	fimo --verbosity 1 \
	    --oc "$OUT"/sampled_hg19_AluSc \
	    "$JASPAR_MATRICES"  \
	    "$OUT"/sampled_hg19_AluSc.fasta	

	Rscript $FIMO_PARSER \
	    "$OUT"/sampled_hg19_AluSc/fimo.txt \
	    "$OUT"/sampled_hg19_AluSc/fimo_parsed.tsv
	
	###

	fastaFromBed -s -fi "$HG19_FASTA" \
	    -bed "$OUT"/sampled_hg19_AluJb.bed \
	    -fo "$OUT"/sampled_hg19_AluJb.fasta		

	fimo --verbosity 1 \
	    --oc "$OUT"/sampled_hg19_AluJb \
	    "$JASPAR_MATRICES"  \
	    "$OUT"/sampled_hg19_AluJb.fasta

	Rscript $FIMO_PARSER \
	    "$OUT"/sampled_hg19_AluJb/fimo.txt \
	    "$OUT"/sampled_hg19_AluJb/fimo_parsed.tsv

	# alignment tests start


	clustalw2 -infile=sampled_hg19_AluJb.fasta \
	    -outfile=sampled_hg19_AluJb.clw \
	    -stats=sampled_hg19_AluJb.stats \
	    -type=dna \
	    -align \
	    -clustering=NJ \
	    -score=percent \
	    -quicktree > sampled_hg19_AluJb.log

	clustalw2 -infile=sampled_hg19_AluSc.fasta \
	    -outfile=sampled_hg19_AluSc.clw \
	    -stats=sampled_hg19_AluSc.stats \
	    -type=dna \
	    -align \
	    -clustering=NJ \
	    -score=percent \
	    -quicktree > sampled_hg19_AluSc.log

	# the same adding the Berta's Alus

	cat sampled_hg19_AluJb.fasta "$ALUJB_FASTA" > sampled_hg19_AluJb_plus_ours.fasta
	cat sampled_hg19_AluSc.fasta "$ALUSC_FASTA" > sampled_hg19_AluSc_plus_ours.fasta	

	clustalw2 -infile=sampled_hg19_AluJb_plus_ours.fasta \
	    -outfile=sampled_hg19_AluJb_plus_ours.clw \
	    -stats=sampled_hg19_AluJb_plus_ours.stats \
	    -type=dna \
	    -align \
	    -clustering=NJ \
	    -score=percent \
	    -quicktree > sampled_hg19_AluJb_plus_ours.log

	clustalw2 -infile=sampled_hg19_AluSc_plus_ours.fasta \
	    -outfile=sampled_hg19_AluSc_plus_ours.clw \
	    -stats=sampled_hg19_AluSc_plus_ours.stats \
	    -type=dna \
	    -align \
	    -clustering=NJ \
	    -score=percent \
	    -quicktree > sampled_hg19_AluSc_plus_ours.log

:<<EOF
	muscle -in sampled_hg19_AluJb.fasta \
	    -out sampled_hg19_AluJb.clw \
	    -clw \
	    -log sampled_hg19_AluJb.log \
	    -maxhours 0.1

	clustalw2 -infile=sampled_hg19_AluJb.fasta \
	    -outfile=sampled_hg19_AluJb.clw \
	    -stats=sampled_hg19_AluJb.stats \
	    -type=dna \
	    -align \
	    -clustering=NJ \
	    -score=percent \
	    -quicktree > sampled_hg19_AluJb.log

        muscle -in sampled_hg19_AluSc.fasta \
	    -out sampled_hg19_AluSc.clw \
	    -clw \
	    -log sampled_hg19_AluSc.log \
	    -maxhours 0.1
EOF
	# alignment tests end
esac
